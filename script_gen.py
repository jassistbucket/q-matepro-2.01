import os.path
from os import path

L=''

import pagelocater
import datetime
#import mysql.connector
from sqlalchemy import create_engine

import automatic_method_pull
import re
def parameter_handle(text):
    string_tag = '\\"([^\\"]*)\\"'
    number_tag = r'(\\\\\\d+)'
    list1=[]   
    string_matches=re.findall(r'\<(.+?)\>',text)
    num_matches=re.findall(r'\d+',text)
    list1.append(len(string_matches))
    list1.append(len(num_matches))
    text=(re.sub('\<(.+?)\>', string_tag, text))
    text=(re.sub("\'(.+?)\'", string_tag, text))
    text=(re.sub(r'\d+', number_tag, text))
    print(list1)
    return(text,list1)
    
    
def no_of_parameters(list1):
    params=[]
    j=0
    for i in range(0,list1[0]):
        j=j+1
        s='String arg'+str(j)
        params.append(s)
    for i in range(0,list1[1]):
        j=j+1
        s='int arg'+str(j)
        params.append(s)
    return params



def javafilecreation(given,when,then,i,Feature):
        script=''
    
        clsname = Feature.replace(' ','')
    
    
        
        L = "import org.openqa.selenium.By;\n" 
        L+="import org.testng.Assert;\n"
        L+="import org.openqa.selenium.WebDriver;\n"
        L+="import org.openqa.selenium.WebElement;\n"
        L+="import org.openqa.selenium.chrome.ChromeDriver;\n"
        L+="import org.openqa.selenium.firefox.FirefoxDriver;\n"
        L+="import cucumber.api.Scenario;\n"
        L+="import cucumber.api.java.After;\n"
        L+="import cucumber.api.java.Before;\n"
        L+="import cucumber.api.java.en.Given;\n"
        L+="import cucumber.api.java.en.Then;\n"
        L+="import cucumber.api.java.en.When;\n"
        L+="public class " + clsname + "{ \n"
        L+="WebDriver driver;\n"
        #before method
        L+="@Before \n public void setUp() throws IOException{\n"
        L+="\tSystem.setProperty(\"webdriver.gecko.driver\", \"path\");\n"
        L+="\t//driver=new ChromeDriver(); \n\tdriver = new FirefoxDriver();\n\t}\n"
        
        #logic for given when den
        gv=""
        wn=""
        dn=""
        gvn=[]
        whn=[]
        thn=[]
        #print (i)
        j=0
        k=0
        l=0
        lenGiven = len(given)
        lenWhen = len(when)
        lenThen = len(then)
        while j < lenGiven:
            given_text,list1=parameter_handle(given[j])
            print('\n\n\ngiven',given_text)
            params=no_of_parameters(list1)
            if(j == 0):
               
                
                gv = given[j].replace('<','')
                
                gv = gv.replace('>','')
                gv=gv.strip()
                gv=gv.replace(' ','_')
                L+="@Given(\"^" + given_text + "$\"){\n//DEFAULT\n public void " + gv + "("+(','.join(params))+") throws IOException{\n"
                L+="\t//driver.get(url);\n\t}\n"
                gvn.append(gv)
            else:
                # if the function is already there in step definition it wont create the same again.
                if not ( given[j].replace(' ','_') in gvn):
                        
                        gv = given[j].replace('<','')
                        gv = gv.replace('>','')
                        gv=gv.strip()
                        gv=gv.replace(' ','_')
                        L+="@Given(\"^" + given_text + "$\"){\n//DEFAULT\n public void " + gv + "("+(','.join(params))+") throws IOException{\n"
                        L+="\t//driver.get(url);\n\t}\n"
                        gvn.append(gv)
                
            j = j + 1                 
                  
        while k < lenWhen:
            when_text,list1=parameter_handle(when[k])
            print('\n\n\n\nwhen',when_text)
            params=no_of_parameters(list1)
            if(k == 0):
                
                
                wn = when[k].replace('<','')
                wn = wn.replace('>','')
                wn=wn.strip()
                wn=wn.replace(' ','_')
                L+="@When(\"^" + when_text + "$\"){\n//DEFAULT\n public void " + wn + "("+(','.join(params))+") throws IOException{\n"
                L+="\t//driver.findElement(By.xpath(xpath)).click();\n\t}\n"
                whn.append(wn)
            else:
                if not ( when[k].replace(' ','_') in whn):
                        
                        wn = when[k].replace('<','')
                        wn = wn.replace('>','')
                        wn=wn.strip()
                        wn=wn.replace(' ','_')
                        L+="@When(\"^" + when_text + "$\"){\n//DEFAULT\n public void " + wn + "("+(','.join(params))+") throws IOException{\n"
                        L+="\t//driver.findElement(By.xpath(xpath)).click();\n\t}\n"
                        whn.append(wn)
            k = k + 1
        while l < lenThen:
            then_text,list1=parameter_handle(then[l])
            params=no_of_parameters(list1)
            if(l == 0):
                
                tn = then[l].replace('<','')
                tn = tn.replace('>','')
                tn=tn.strip()
                tn=tn.replace(' ','_')
                L+="@Then(\"^" + then_text + "$\"){\n//DEFAULT\n public void " + tn + "("+(','.join(params))+") throws IOException{\n"
                L+="\t//driver.findElement(By.xpath(xpath)).click();\n\t}\n"
                thn.append(tn)
            else:
                if not ( then[l].replace(' ','_') in thn):
                        
                        tn = then[l].replace('<','')
                        tn = tn.replace('>','')
                        tn=tn.strip()
                        tn=tn.replace(' ','_')
                        L+="@Then(\"^" + then_text + "$\"){\n//DEFAULT\n public void " + tn + "("+(','.join(params))+") throws IOException{\n"
                        L+="\t//driver.findElement(By.xpath(xpath)).click();\n\t}\n"
                        thn.append(tn)
            l = l + 1         
       
        
        #after method
        L+="@After\n public static void tearDown(){\n"
        L+="\t//driver.quit()\n\t}\n}"
        
        
        
        final_given,final_when,final_then=automatic_method_pull.automatic_main(L)
        print('\n\n\nfinalgiven',final_given,'\n\n\nfinalwhen',final_when)
        script={"GIVEN":final_given,"WHEN":final_when,"THEN":final_then}
        
        return script
    
        
        
        


        
def finaljavafilecreation(given,when,then,given_func,when_func,then_func,Feature):
    script=''
    PL=''
    PA=[]
    
    clsname = Feature.replace(' ','')
     
    L ="package steps;\nimport org.openqa.selenium.By;\n"
    L+="import org.testng.Assert;\n"
    L+="import org.openqa.selenium.WebDriver;\n"
    L+="import org.openqa.selenium.WebElement;\n"
    L+="import org.openqa.selenium.chrome.ChromeDriver;\n"
    L+="import org.openqa.selenium.firefox.FirefoxDriver;\n"
    L+="import cucumber.api.Scenario;\n"
    L+="import cucumber.api.java.After;\n"
    L+"import cucumber.api.java.Before;\n"
    L+="import cucumber.api.java.en.Given;\n"
    L+="import cucumber.api.java.en.Then;\n"
    L+="import cucumber.api.java.en.When;\n"
    L+="public class " + clsname + "{ \n"
    L+="WebDriver driver;\n"
    #before method
    L+="\n@Before \n public void setUp() throws IOException{\n"
    L+="\tSystem.setProperty(\"webdriver.gecko.driver\", \"path\");\n"
    L+="\t//driver=new ChromeDriver(); \n\tdriver = new FirefoxDriver();\n\t}\n"
        
    #logic for given when den
    gv=""
    wn=""
    dn=""
    gvn=[]
    whn=[]
    thn=[]
    data=pagelocater.read_data()
    #dd=data['J_CON'].values.tolist()
    #print (i)
    
    k=0
    l=0
    lenGiven = len(given)
    lenWhen = len(when)
    lenThen = len(then)
    #print('given_func\n\n\n\n',given_func)
    for key in given_func:
        j=0
        key2=key
        
        while j < lenGiven:
            given_text,list1=parameter_handle(given[j])
            print(key2,key)
            
            key=key.replace('Given("^','')
            key=key.strip()
            key=key.replace('$"){','')
            key=key.strip()
            given_text=given_text.strip(':')
            given_text=given_text.strip()
            print(j,'Key:',key,'given:',given_text)
            if key==given_text:
                print("\n\n\nGIVEN FUNC KEY2",key)
                split_str=key.split('\\"([^\\"]*)\\"')
                #print("split_str",split_str)
                if '//DEFAULT' not in given_func[key2]:
                    func_def=pagelocater.get_the_path(key2,given_func[key2])
                    PA.append(func_def)
                print('sawaw',key)
                if(j == 0):
                    print('k0',k)
                    
                    gv = given_text.replace('<','')
                    gv = gv.replace('>','')
                    gv=gv.replace(' ','_')
                    L+="@Given(\"^" + given_text + "$\")\n"+given_func[key2]+"\n"
                    gvn.append(gv)
                else:
                    # if the function is already there in step definition it wont create the same again.
                    if not ( given_text.replace(' ','_') in gvn):
                        
                        gv = given_text.replace('<','')
                        gv = gv.replace('>','')
                        gv=gv.replace(' ','_')
                        L+="@Given(\"^" + given_text + "$\")\n"+given_func[key2]+"\n"
                        gvn.append(gv)
                
            j = j + 1                 
    for key in when_func:
        k=0
        key2=key
        print('key is\n\n\n',key)
        #print('cvnvbnv',type(key))
        while k < lenWhen:
            when_text,list1=parameter_handle(when[k])
            
            key=key.replace('When("^','')
            #print('key is',key)
            key=key.strip()
            #print('key is',key)
            key=key.replace('$"){','')
            #print('key is',key)
            key=key.strip()
            #print('key is',key)
            #key=key.strip(' ')
            when_text=when_text.strip(':')
            when_text=when_text.strip()
            #print(when_text,key)
            if key==when_text:
                if '//DEFAULT' not in when_func[key2]:
                    func_def=pagelocater.get_the_path(key2,when_func[key2])
                    PA.append(func_def)
                if(k == 0):
                    #print('k0',k)
                    
                    wn = when_text.replace('<','')
                    wn = wn.replace('>','')
                    wn=wn.replace(' ','_')
                    L+="@When(\"^" + when_text + "$\")\n"+when_func[key2]+"\n"
                    whn.append(wn)
                else:
                    if not ( when_text.replace(' ','_') in whn):
                        print('else k0',k)
                        
                        wn = when_text.replace('<','')
                        wn = wn.replace('>','')
                        wn=wn.replace(' ','_')
                        L+="@When(\"^" + when_text + "$\")\n"+when_func[key2]+"\n"
                        whn.append(wn)
            k = k + 1

    for key in then_func:
        l=0
        key2=key
        while l < lenThen:
            then_text,list1=parameter_handle(then[l])
            key=key.replace('Then("^','')
            key=key.strip()
            key=key.replace('$"){','')
            key=key.strip()
            #key=key.strip(' ')
            then_text=then_text.strip(':')
            then_text=then_text.strip()
            if key==then_text:
                if '//DEFAULT' not in then_func[key2]:
                    func_def=pagelocater.get_the_path(key2,then_func[key2])
                    PA.append(func_def)
                if(l == 0):
                    
                    tn = then_text.replace('<','')
                    tn = tn.replace('>','')
                    tn=tn.replace(' ','_')
                    L+="@Then(\"^" + then_text + "$\")\n "+then_func[key2]+"\n"
                
                    thn.append(tn)
                else:
                    if not ( then_text.replace(' ','_') in thn):
                       
                        tn = then_text.replace('<','')
                        tn = tn.replace('>','')
                        tn=tn.replace(' ','_')
                        L+="@Then(\"^" + then_text + "$\")\n "+then_func[key2]+"\n"
                        
                        thn.append(tn)
            l = l + 1    
        
        
        #after method
    L+="\n@After\n public static void tearDown(){\n"
    L+="\t//driver.quit()\n\t}\n}"
    

    script=L
    print("PAGELOCA|TOR \n\n\n")
    print(PL)
    print(PA)
    return script,PA
    
        

def script_gen_main(text,nam):
    Feature=nam
    
    i=0
    scenario=[]
    given=[]
    when=[]
    then=[]
    from textblob import TextBlob
    #text=text.lower()
    l=text.split('Scenario')
    
    for i in l:
        given_flag=0
        when_flag=0
        then_flag=0
        scenario_flag=0
        when_string=''
        then_string=''
        given_string=''
        scenario_string=''
        blob = TextBlob(i)
        for sentences in blob.sentences:
            for word,tag in sentences.tags:
                if word=='Scenario':
                    scenario_flag=1
                    given_flag=0
                    when_flag=0
                    then_flag=0
                if word=='Given':
                    given_flag=1
                    when_flag=0
                    then_flag=0
                    scenario_flag=0
                if word=='When' or word=='when':
                    when_flag=1
                    then_flag=0
                    given_flag=0
                    scenario_flag=0
                if word=='Then' or word=='then':
                    when_flag=0
                    then_flag=1
                    given_flag=0
                    scenario_flag=0
                if scenario_flag==1:
                    scenario_string=scenario_string+' '+word
                if when_flag==1 and word!='When':
                    when_string=when_string+' '+word
                if then_flag==1 and word!='Then':
                    then_string=then_string+' '+word
                if given_flag==1 and word!='Given':
                    given_string=given_string+' '+word
        when.append(when_string)
        then.append(then_string)
        given.append(given_string)
        
        scenario.append(scenario_string)
    
    
    when=list(filter(None,when))

    then=list(filter(None,then))
    given=list(filter(None,given))
    scenario=list(filter(None,scenario))
    
    i=len(scenario)
    
    final_when=[]
    eg_flag=0
    for w in when:
        #temp_w=[]
        for i in w.split('And'):
            if 'Example' in i or 'Examples' in i:
                j=''
                for split_words in i.split():
                    if split_words=='Example' or split_words=='Examples':
                        break
                    else:
                        j=j+split_words+' '
                final_when.append(j)
                eg_flag=1
            if eg_flag==1:
                break
            else:
                final_when.append(i)
        #final_when.append(temp_w)
        
    final_then=[]
    eg_flag=0
    for t in then:
        #temp_t=[]
        for i in t.split('And'):
            print('\n\n\n\n\n\nthen',i)
            if 'Example' in i or 'Examples' in i:
                print('\n\n\n\n\n\neg found\n\n\n\n')
                j=''
                for split_words in i.split():
                    if split_words=='Example' or split_words=='Examples':
                        break
                    else:
                        j=j+split_words+' '
                final_then.append(j)
                eg_flag=1
            if eg_flag==1:
                break
            else:
                final_then.append(i)
        #final_then.append(temp_t)
    
    final_given=[]
    eg_flag=0
    for g in given:
        #temp_g=[]
        for i in g.split('And'):
            if 'Example' in i or 'Examples' in i:
                j=''
                for split_words in i.split():
                    if split_words=='Example' or split_words=='Examples':
                        break
                    else:
                        j=j+split_words+' '
                final_given.append(j)
                eg_flag=1
            if eg_flag==1:
                break
            else:  
                final_given.append(i)
        #final_given.append(temp_g)
        
    
    script=javafilecreation(final_given,final_when,final_then,i,Feature)
    
    #script=finaljavafilecreation(javapath,given,when,then,given_func,when_func,then_func,i,Feature)
    
    return script
    
    

def script_gen_main_final(text,nam,given_func,when_func,then_func):
    
    Feature=nam
    
    i=0
    scenario=[]
    given=[]
    when=[]
    then=[]
    from textblob import TextBlob
    #text=text.lower()
    l=text.split('Scenario')
    
    for i in l:
        given_flag=0
        when_flag=0
        then_flag=0
        scenario_flag=0
        when_string=''
        then_string=''
        given_string=''
        scenario_string=''
        blob = TextBlob(i)
        for sentences in blob.sentences:
            for word,tag in sentences.tags:
                if word=='Scenario' or word=='scenario':
                    scenario_flag=1
                    given_flag=0
                    when_flag=0
                    then_flag=0
                
                if word=='Given' or word=='given':
                    given_flag=1
                    when_flag=0
                    then_flag=0
                    scenario_flag=0
                if word=='When' or word=='when':
                    when_flag=1
                    then_flag=0
                    given_flag=0
                    scenario_flag=0
                if word=='Then' or word=='then':
                    when_flag=0
                    then_flag=1
                    given_flag=0
                    scenario_flag=0
                if scenario_flag==1:
                    scenario_string=scenario_string+' '+word
                if when_flag==1 and word!='When':
                    when_string=when_string+' '+word
                if then_flag==1 and word!='Then':
                    then_string=then_string+' '+word
                if given_flag==1 and word!='Given':
                    given_string=given_string+' '+word
        when.append(when_string)
        then.append(then_string)
        given.append(given_string)
        scenario.append(scenario_string)
    
    
    when=list(filter(None,when))

    then=list(filter(None,then))
    given=list(filter(None,given))
    scenario=list(filter(None,scenario))
    
    i=len(scenario)
    final_when=[]
    eg_flag=0
    for w in when:
        #temp_w=[]
        for i in w.split('And'):
            if 'Example' in i or 'Examples' in i:
                j=''
                for split_words in i.split():
                    if split_words=='Example' or split_words=='Examples':
                        break
                    else:
                        j=j+split_words+' '
                final_when.append(j)
                eg_flag=1
            if eg_flag==1:
                break
            else:
                final_when.append(i)
        #final_when.append(temp_w)
        
    final_then=[]
    eg_flag=0
    for t in then:
        #temp_t=[]
        for i in t.split('And'):
            print('\n\n\n\n\n\nthen',i)
            if 'Example' in i or 'Examples' in i:
                print('\n\n\n\n\n\neg found\n\n\n\n')
                j=''
                for split_words in i.split():
                    if split_words=='Example' or split_words=='Examples':
                        break
                    else:
                        j=j+split_words+' '
                final_then.append(j)
                eg_flag=1
            if eg_flag==1:
                break
            else:
                final_then.append(i)
        #final_then.append(temp_t)
    
    final_given=[]
    eg_flag=0
    for g in given:
        #temp_g=[]
        for i in g.split('And'):
            if 'Example' in i or 'Examples' in i:
                j=''
                for split_words in i.split():
                    if split_words=='Example' or split_words=='Examples':
                        break
                    else:
                        j=j+split_words+' '
                final_given.append(j)
                eg_flag=1
            if eg_flag==1:
                break
            else:  
                final_given.append(i)
        #final_given.append(temp_g)
    #print('final given',final_given)
    script,PA=finaljavafilecreation(final_given,final_when,final_then,given_func,when_func,then_func,Feature)
    return script,PA
    




