import numpy as np
import pandas as pd
#import pika
import dep_parse
from whoosh.index import create_in
from whoosh.fields import *
import os.path

from whoosh.index import open_dir

from whoosh.analysis import StemmingAnalyzer

from whoosh.qparser import QueryParser,MultifieldParser
import requests
import json
#from sqlalchemy import create_engine
#import mysql.connector
import urllib
from difflib import SequenceMatcher
from whoosh.query import FuzzyTerm
response_slug=[]
branches=[]


def get_branches(ACCESS_TOKEN):

    src1='https://api.bitbucket.org/2.0/repositories/cardsproject2019'+ACCESS_TOKEN
    fp1 = requests.get(src1,auth=('shovona.sarkar@gmail.com','password'),verify=False)
    mystr1=json.loads(fp1.text)
    #print(mystr1)
    response_i=mystr1["values"]
    for r in response_i:
        response_slug.append(r['slug'])
    #print(response_slug)
    for k in response_slug:
        print(k)
        src2='https://api.bitbucket.org/2.0/repositories/cardsproject2019/'+k+'/refs/branches'+ACCESS_TOKEN
        print(src2)
        fp2 = requests.get(src2,auth=('shovona.sarkar@gmail.com','password'),verify=False)
        mystr2=json.loads(fp2.text)
        response_i2=mystr2['values']
        #print(response_i2)
        for r in response_i2:
            #print('ss',r['name'])
            d={k:r['name']}

            branches.append(d)
    return branches
def get_dataframe():
    
    import requests
    sj=''
    import json
    ACCESS_TOKEN='?access_token=N-1lS89sVJ66zdnPG6XV9JYshfBPGCGEIIf4Hrx7o-O64hRxbRxzFsCk787wDrzTc_WYyJYexUQNlvXVVQ%3D%3D'

    df=pd.DataFrame()
    b=get_branches(ACCESS_TOKEN)
    #print(b)

    for i in b:
            for key,values in i.items():
                #print(key,values)
                src='https://api.bitbucket.org/1.0/repositories/cardsproject2019/'+key+'/src/'+values+'/'+ACCESS_TOKEN
                fp = requests.get(src,auth=('shovona.sarkar@gmail.com','password'),verify=False)
                mystr=(fp.text)
                mystr=json.loads(mystr)
                #print(mystr)
                direc=mystr['directories']
                #print(direc)
                if not direc:
                    pass
                else:
                    for u in direc:
                        sj='https://api.bitbucket.org/1.0/repositories/cardsproject2019/'+key+'/src/'+values+'/'+u+ACCESS_TOKEN
                        fj = requests.get(sj,auth=('shovona.sarkar@gmail.com','password'),verify=False)
                        my=(fj.text)
                        my=json.loads(my)
                        w=(my['files'])
                        for folder in w:

                            sr='https://api.bitbucket.org/1.0/repositories/cardsproject2019/'+key+'/src/'+values+'/'+(folder['path'])+ACCESS_TOKEN
                            print(sr)
                            fx = requests.get(sr,auth=('shovona.sarkar@gmail.com','password'),verify=False)
                            myx=(fx.text)
                            myx=json.loads(myx)
                            code1=(myx['data'])
                            filename1=(myx['path'])
                            pth1=sr.replace("api.bitbucket.org/1.0/repositories", "bitbucket.org")
                            #print(pth)
                            d1={'filename':filename1,'code':code1,'path':pth1}
                            df = df.append(d1, ignore_index=True)
                    print(df['filename'])
                mystr=(mystr['files'])

                for mystring in mystr:

                    src2='https://api.bitbucket.org/1.0/repositories/cardsproject2019/'+key+'/src/'+values+'/'+(mystring['path'])+ACCESS_TOKEN
                    #print(src2)
                    fp2 = requests.get(src2,auth=('shovona.sarkar@gmail.com','password'),verify=False)
                    mystr2=(fp2.text)
                    mystr2=json.loads(mystr2)
                    code=(mystr2['data'])
                    filename=(mystr2['path'])
                    pth=src2.replace("api.bitbucket.org/1.0/repositories", "bitbucket.org")
                    #print(pth)
                    d={'filename':filename,'code':code,'path':pth}
                    df = df.append(d, ignore_index=True)
    data = df
    print(data['filename'])
    data = data.fillna(' ')
    data['filename'] = data['filename'].astype(str)
    data['code'] = data['code'].astype(str)
    data['path'] = data['path'].astype(str)
    data=data[['filename','code','path']]
    conn,engine=db_connection()
    data.to_sql(name='bitbot', con=engine, if_exists = 'replace', index=False)

    print("Data Read DB")
    #return data
def read_data():
    
    

    data= pd.read_excel("JAVAData.xls")
    data['repo'] = data['repo'].astype(str)
    data['filename'] = data['filename'].astype(str)
    #data['key'] = data['key'].str.lower()
    data['code'] = data['code'].astype(str)
    data['code'] = data['code'].str.lower()
    data['path'] = data['path'].astype(str)
    
	
    
    #data = preprocess_status(data)
    print(" [X] data read succesfully...")
    print()
    print(data.info())

    return data
def create_schema():

    schema = Schema(repo=TEXT(stored=True),filename=TEXT(stored=True), code=TEXT(analyzer=StemmingAnalyzer()), path=TEXT(analyzer=StemmingAnalyzer()))

    if not os.path.exists("index2"):
        os.mkdir("index2")
    ix = create_in("index2", schema)
    print("Schema Created")
    return None
def create_index(data):

    ix = open_dir("index2")
    writer = ix.writer()

    for index, row in data.iterrows():
        writer.add_document(repo=row['repo'],filename=row['filename'], code=row['code'], path = row['path'])

    writer.commit()
    print("INDEX CREATED")

    return ix            
def get_results(query):

    searcher = ix.searcher()


    qp = MultifieldParser(["code", "filename"], schema=ix.schema)

    ToDisplay=''

    result=[]
    d={}

    q = qp.parse(query)
    print('parsing')

    with ix.searcher() as s:
        print('searching')
        results = s.search(q)
        print(results.scored_length())
        print(results.scored_length())
        if results.scored_length() == 0:
            print('empty')
            FNAM="No Match Found"
            CODE="No Match Found"
            PTH="No Match Found"
            REPO="No Match Found"
            d={'Repo':REPO,'File Name': FNAM,'Path':PTH,'Code':CODE}
            result.append(d)
            print(result)
            return result

        else:
            print('results=\n',results)
            print('ok')

            for hit in results:
                        REPO=str(set((data.repo[data.filename == hit['filename']]).values))
                        FNAM = str(set((data.filename[data.filename == hit['filename']]).values))
                        FNAM=FNAM.strip(" '{}'")
                        CODE = str(set((data.code[data.filename == hit['filename']]).values))
                        CODE=CODE.strip(" '{}'")
                        PTH = str(set((data.path[data.filename == hit['filename']]).values))
                        
                        PTH=PTH.strip(" '{}'")
                        PTH=PTH.split("?access_token")
                        PTH=PTH[0]
                        PTH=PTH.replace("api.bitbucket.org/2.0/repositories", "bitbucket.org")
                        #PTH=PTH.replace(' ','-')
                        d={'File Name': FNAM,'Path':PTH,'Code':CODE,'Repo':REPO}
                        result.append(d)


            return result






def get_results2(query,query2):
    print("Getting Result")
    print(query)
    ToDisplay=''
    duplicates=[]
    result=[]
    func={}
    searcher = ix.searcher()

    qp = MultifieldParser(["code", "filename"], schema=ix.schema,termclass=MyFuzzyTerm)
    #qp.add_plugin(qparser.FuzzyTermPlugin())
    q = qp.parse(query)
    print('parsed')
    paren=(qp.parse(query2))
    with ix.searcher() as s:
        print('inside searcher')
        results = s.search(q,limit=None)
        print('len of results')
        print(results.scored_length())
        #print(results[0])
        if results.scored_length()==0:
            print("0 Results")
            FNAM="No Match Found"
            FUNC="No Match Found"
            LINE="No Match Found"
            
            PTH="No Match Found"
            REPO="No Match Found"
            d={'Repo':REPO,'File Name': FNAM,'Path':PTH,'Function':FUNC,'Line No':LINE}
            if PTH not in duplicates:
                duplicates.append(PTH)
                result.append(d)
            print(result)
            return result
            
        else:
            print("foudn Results")
            for hit in results:
                        REPO=str(set((data.repo[data.filename == hit['filename']]).values))
                        FNAM = str(set((data.filename[data.filename == hit['filename']]).values))
                        FNAM=FNAM.strip(" '{}'")
                        CODE = str(set((data.code[data.filename == hit['filename']]).values))
                        CODE=CODE.strip(" '{}'")
                        #CODE2=CODE
                        PTH = str(set((data.path[data.filename == hit['filename']]).values))
                        
                        PTH=PTH.strip(" '{}'")
                        PTH=PTH.split("?access_token")
                        PTH=PTH[0]
                        PTH=PTH.replace("api.bitbucket.org/2.0/repositories", "bitbucket.org")
                        
                        
                        
                       
                        CODE=CODE.strip('""')
                        #print(Des)
                        CODE=CODE.replace('\\\\','\\')
                        #print(str(Des))
                        Des2=CODE
                        
                        CODE = CODE.replace('\\n', ' ')
                        
                        
                
                        #print(Des)
                        results2=re.findall(r'\S+(?=\()',CODE )
                        #print(results2)
                        for hit2 in results2:
                            #print('bb')
                            #print(hit2)
                            #print(paren)
                            #print('\n\n\n')
                            hit2=hit2+'('
                            length=Des2.find(hit2)
                            new_string=Des2[0:length]
                            #print(Des2)
                            #print(re.findall(r'\"([^"]*)\"', new_string))
                            line_no=(new_string.count('\\n'))
                            if line_no==0:
                                line_no=1
                            #print(hit2)
                            ratio=similar(hit2,query2)
                            #hit2=hit2+'('
                            if ratio>0.5:
                                print('aa')
                                LINE_NO=str(line_no+1)
                                NAM=FNAM.split('/')
                                NAM=(NAM[-1])
                                VI='?fileviewer=file-view-default#'
                                PTH=PTH+VI+NAM+'-'+LINE_NO
                                #print(PTH)
                                hit2=hit2.split('(')
                                hit2=hit2[0]
                                d={'Repo':REPO,'File Name':FNAM,'Path':PTH,'Function':hit2,'Line No':line_no+1}
                                if PTH not in duplicates:
                                    duplicates.append(PTH)
                                    result.append(d)
                            
                                
                                
                        
            #print(result)
            if result==[]:
                d={'Repo':'No Match Found','File Name': 'No Match Found','Path':'No Match Found','Function':'No Match Found','Line No':'No Match Found'}
                result.append(d)
            return result


class MyFuzzyTerm(FuzzyTerm):
     def __init__(self, fieldname, text, boost=1.0, maxdist=10, prefixlength=0, constantscore=True):
         super(MyFuzzyTerm, self).__init__(fieldname, text, boost, maxdist, prefixlength, constantscore)



def similar(a, b):
    return SequenceMatcher(None, a, b).ratio()




#conn,engine=db_connection()

data=read_data()
#data=data[~data.code.str.contains('{"type":"error","error":{"message"')]
#create_schema()
ix=create_index(data)

#ix = open_dir("index2")


