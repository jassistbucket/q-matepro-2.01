import pymongo
from datetime import date
client = pymongo.MongoClient("localhost", 27017)
db = client.qmateproDB
def add_items(PageAct_Text,PageLoc_Text,filename_text):
     

     if(PageLoc_Text!=""):
        if(len([m for m in db.mycart.find()]))==0:
            ID=len([m for m in db.mycart.find()])+1
        else:
            ID=[m for m in db.mycart.find()][-1]["ID"]+1
        print("Page Locator")
        mydict={"ID":ID,"filename":filename_text+"_Pagelocators","content":PageLoc_Text,"date":str(date.today())}
        db.mycart.insert_one(mydict)
        
     if(PageAct_Text!=""):
         if(len([m for m in db.mycart.find()]))==0:
            ID=len([m for m in db.mycart.find()])+1
         else:
            ID=[m for m in db.mycart.find()][-1]["ID"]+1
         print("Page Action")
         mydict={"ID":ID,"filename":filename_text+"_Pageactions","content":PageAct_Text,"date":str(date.today())}
         db.mycart.insert_one(mydict)
     return "DONE"
     
def add_items_script(script,filename_text):
     

         if(len([m for m in db.mycart.find()]))==0:
            ID=len([m for m in db.mycart.find()])+1
         else:
            ID=[m for m in db.mycart.find()][-1]["ID"]+1
         print("Page Action")
         mydict={"ID":ID,"filename":filename_text+"","content":script,"date":str(date.today())}
         db.mycart.insert_one(mydict)
         return "DONE"
     


def get_items():
    cartitems=[]
    for item in db.mycart.find():
         filename=(item["filename"])
         content=(item["content"])
         date=(item["date"])
         ID=(item["ID"])
         D={"ID":ID,"nam":filename,"content":content,"date":date}
         cartitems.insert(0,D)
    return cartitems
def edit_items(ID,nam_new,con_new):
    db.mycart.update_one({'ID':int(ID)},
                                {'$set': {'filename': nam_new,'content':con_new,'date':str(date.today())}}, 
                                upsert=True)
    return "EDITED"
def del_items(ID):
    db.mycart.delete_one({'ID': int(ID)})
    return "DELETED"
  