package PageActions;

import PageLocators.WebPageTestLocators;

public class WebPageTestActions {
	WebPageTestLocators googlePageElement = new WebPageTestLocators();

	public String get_h2Header() throws InterruptedException {
		return googlePageElement.h2_header.getText();

	}

}
