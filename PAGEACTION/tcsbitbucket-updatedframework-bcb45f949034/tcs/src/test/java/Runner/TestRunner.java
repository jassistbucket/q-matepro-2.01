package Runner;

import cucumber.api.CucumberOptions;

import cucumber.api.testng.AbstractTestNGCucumberTests;

@CucumberOptions(
		plugin= {"json:target/cucumber1.json","pretty","html:target/cucumber.html"},
		features = "src/test/java/Features",
		glue = "StepDef",
		tags = "@demo"
		//monochrome=true
		
		)
public class TestRunner extends AbstractTestNGCucumberTests
{

	
}
