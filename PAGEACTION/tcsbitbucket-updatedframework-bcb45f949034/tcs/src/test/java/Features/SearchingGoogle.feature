Feature: Demo TestCase
@demo
  Scenario Outline: User searches a text in Web Page Test Site
    Given user opens a "<url>" website   
    And search for "<SearchText>" and verify

 Examples:
 |url|SearchText|
 |https://www.webpagetest.org|Test a website's performance|