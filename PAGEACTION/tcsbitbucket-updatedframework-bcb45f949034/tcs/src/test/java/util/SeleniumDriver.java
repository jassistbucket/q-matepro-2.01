package util;

import java.io.File;
import java.util.concurrent.TimeUnit;

import org.apache.commons.io.FileUtils;
import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.log4j.Logger;
import org.apache.log4j.PropertyConfigurator;
import org.apache.log4j.xml.DOMConfigurator;
import org.openqa.selenium.OutputType;
import org.openqa.selenium.TakesScreenshot;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.WebDriverException;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.chrome.ChromeOptions;
import org.openqa.selenium.support.ui.WebDriverWait;

import com.aventstack.extentreports.gherkin.model.Scenario;


public class SeleniumDriver {

	private static SeleniumDriver seleniumDriver;
	//String driverName = "phantomjsdriver";
	public static WebDriver driver;	 
	final static Log log=LogFactory.getLog(SeleniumDriver.class.getName());
	//Initialize timeouts
	public static WebDriverWait waitdriver;
	public final static int TIMEOUT = 60;
	public final static int PAGE_LOAD_TIMEOUT = 60;
	ChromeOptions options = new ChromeOptions();
	ConfigFileReader configFileReader;
	private SeleniumDriver()
	{
		configFileReader= new ConfigFileReader();
		int countRetry=0;
		int maxRetry=3;
		//PropertyConfigurator.configure("log4j.properties");		
		try {
			
			while(driver==null && countRetry<maxRetry)
			{				
				System. setProperty("webdriver.chrome.driver", "F:\\chromedriver.exe");		
				driver = new ChromeDriver();
				driver.manage().window().maximize();
				driver.manage().timeouts().implicitlyWait(TIMEOUT, TimeUnit.SECONDS);
				driver.manage().timeouts().pageLoadTimeout(PAGE_LOAD_TIMEOUT, TimeUnit.SECONDS);
				countRetry++;
				log.info("Driver initiated");
				
			}
		} 
		catch (Exception e) {
			if(countRetry==maxRetry)
			{
				throw new WebDriverException(e.getMessage());
			}
		}
	}
	public static void setUpDriver()
	{
		if(seleniumDriver == null)
			seleniumDriver = new SeleniumDriver();
	}
	public static void openPage(String url)
	{
		//log.info(url);
		//log.info(driver);
		driver.get(url);
	}
	
	public static WebDriver getDriver()
	{
		return driver;
	}
	
	
	public static void tearDown()
	{
		if(driver != null) {
			driver.quit();
			log.info("Ending test case");
		driver = null;
		}
	}
}


