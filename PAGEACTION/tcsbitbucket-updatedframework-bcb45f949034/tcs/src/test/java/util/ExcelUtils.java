package util;

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.text.SimpleDateFormat;
import java.util.Date;

import org.apache.commons.logging.Log;
import org.apache.commons.logging.LogFactory;
import org.apache.poi.hssf.usermodel.HSSFRow;
import org.apache.poi.hssf.usermodel.HSSFSheet;
import org.apache.poi.hssf.usermodel.HSSFWorkbook;
import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.DataFormatter;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.usermodel.Sheet;
import org.apache.poi.ss.usermodel.Workbook;
import org.apache.poi.ss.usermodel.WorkbookFactory;
import org.apache.poi.xssf.usermodel.XSSFCell;
import org.apache.poi.xssf.usermodel.XSSFRow;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;
import org.junit.Assert;

public class ExcelUtils {
	// Main directory of the project
	public static final String currentDir = System.getProperty("user.dir");
	final static Log log = LogFactory.getLog(ExcelUtils.class);

	public static ConfigFileReader configFileReader;
	// Location of test data excel file
	public static String testDataExcelPath = null;
	// Excel Workbook
	public static XSSFWorkbook excelWBook;
	// Excel Sheet
	public static XSSFSheet excelWSheet;
	// Excel Cell
	public static XSSFCell cell;
	// Excel Row
	public static XSSFRow row;
	// Row number
	public static int rowNumber;
	// Column number
	public static int columnNumber;
	public static String testDataExcelFileName = "";
	public static String testDataExcelSheetName = "";

	// Content validation variables
	private static String contentDataExcelPath;
	private static String contentDataExcelName;
	private static String contentDataExcelSheet;
	private static String contentDataPath;
	private static XSSFWorkbook contentExcelWBook;
	private static XSSFSheet contentExcelWSheet;
	private static XSSFRow contentRow;

	// Setter and getter of rows and columns
	public static int getRowNumber() {
		return rowNumber;
	}

	public static void setRowNumber(int pRowNumber) {
		rowNumber = pRowNumber;
	}

	public static int getColumnNumber() {
		return columnNumber;
	}

	public static void setColumnNumber(int pColumnNumber) {
		columnNumber = pColumnNumber;
	}

	public static void setExcelFileSheet(String sheetName) {
		configFileReader = new ConfigFileReader();
		testDataExcelFileName = "";
		testDataExcelFileName = configFileReader.getExcelName();
		testDataExcelSheetName = "Demo";
		testDataExcelPath = currentDir + configFileReader.getExcelPath();
		log.info(testDataExcelPath + testDataExcelFileName);
		FileInputStream ExcelFile = null;
		try {
			ExcelFile = new FileInputStream(testDataExcelPath + testDataExcelFileName);
		} catch (FileNotFoundException e1) {
			e1.printStackTrace();
		}

		try {
			excelWBook = new XSSFWorkbook(ExcelFile);
		} catch (IOException e) {
			e.printStackTrace();
		}
		excelWSheet = excelWBook.getSheet(sheetName);
	}

	public static String getCellData(int RowNum, int ColNum) {
		log.info(RowNum + " " + ColNum);
		cell = excelWSheet.getRow(RowNum).getCell(ColNum);
		DataFormatter formatter = new DataFormatter();
		String cellData = formatter.formatCellValue(cell);
		return cellData;
	}

	public static XSSFRow getRowData(int RowNum) throws Exception {
		log.info("Iteration is " + RowNum);
		try {
			row = excelWSheet.getRow(RowNum);
			return row;
		} catch (Exception e) {
			throw (e);
		}
	}

	public static void setCellData(String value, int RowNum, int ColNum) {
		row = excelWSheet.getRow(RowNum);
		cell = row.getCell(ColNum);
		if (cell == null) {
			cell = row.createCell(ColNum);
			cell.setCellValue(value);
		} else {
			cell.setCellValue(value);
		}

		FileOutputStream fileOut = null;
		try {
			fileOut = new FileOutputStream(testDataExcelPath + testDataExcelFileName);
		} catch (FileNotFoundException e) {
			e.printStackTrace();
		}
		try {
			excelWBook.write(fileOut);
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			fileOut.flush();
		} catch (IOException e) {
			e.printStackTrace();
		}
		try {
			fileOut.close();
		} catch (IOException e) {
			e.printStackTrace();
		}
	}

	public void CreateExlFile(String filePath) {
		try {
			String fileName = filePath;
			File file = new File(filePath);
			if (!file.exists()) {
				HSSFWorkbook workbook = new HSSFWorkbook();
				SimpleDateFormat sdf = new SimpleDateFormat("ddMMyyyy_hhmmss");
				Date curDate = new Date();
				String strDate = sdf.format(curDate);
				HSSFSheet sheet = workbook.createSheet(strDate);
				HSSFRow rowhead = sheet.createRow((short) 0);
				row.createCell(0).setCellValue("No.");
				row.createCell(1).setCellValue("US#");
				row.createCell(2).setCellValue("Scenario");
				row.createCell(3).setCellValue("Feasible");
				row.createCell(4).setCellValue("Status");
				row.createCell(5).setCellValue("Comments");
				row.createCell(6).setCellValue("ALM test script id");
				sheet.autoSizeColumn(5);
				FileOutputStream fileout = new FileOutputStream(fileName);
				workbook.write(fileout);
				fileout.close();
				System.out.println("Your excel file has been generated!");
				log.info(">> Your excel file has been generated!");
			} else {
				System.out.println("Your excel file has already been generated!");
				log.info(">> Your excel file has already been generated!");
			}
		} catch (Exception ex) {
			System.out.println(ex);
			log.info(">> Error on excel creation" + ex);
		}
	}

	public void writeExlFile(String scegetName, String scegetStatus, Boolean sceisFailed, String filePath) {
		try {
			FileInputStream myxls = new FileInputStream(filePath);
			HSSFWorkbook bddTestResultSheet = new HSSFWorkbook(myxls);
			HSSFSheet worksheet = bddTestResultSheet.getSheetAt(0);
			int a = worksheet.getLastRowNum();
			System.out.println(a);

			Row row = worksheet.createRow(++a);
			row.createCell(0).setCellValue(a);
			worksheet.autoSizeColumn(0);
			String string = scegetName;
			String[] parts = string.split(":");
			row.createCell(1).setCellValue(parts[1].trim());
			worksheet.autoSizeColumn(1);
			row.createCell(2).setCellValue(parts[0].trim());
			worksheet.autoSizeColumn(2);

			if (parts[0].trim().contains("Non Feasible")) {
				row.createCell(3).setCellValue("Not feasible to automate");
				row.createCell(4).setCellValue("-");
				row.createCell(5).setCellValue("Manually tested as this is not feasible to automate");
			} else if (parts[0].trim().contains("Spillover")) {
				row.createCell(3).setCellValue("-");
				row.createCell(4).setCellValue("-");
				row.createCell(5).setCellValue("Spillover to next sprint");
			} else {
				row.createCell(3).setCellValue("-");
				row.createCell(4).setCellValue(scegetStatus.trim());
				if (parts[0].trim().contains("Partial Automated")) {
					row.createCell(5).setCellValue(
							"Only part of the scenario is automated, remaining validation will be done manually");
				} else {
					row.createCell(5).setCellValue(System.getProperty("Data"));
				}
			}
			worksheet.autoSizeColumn(3);
			worksheet.autoSizeColumn(4);
			worksheet.autoSizeColumn(5);

			myxls.close();
			FileOutputStream output_file = new FileOutputStream(new File(filePath));
			// write changes
			bddTestResultSheet.write(output_file);
			output_file.close();
			System.out.println(" is successfully written");
			log.info(">> is successfully written");
		} catch (Exception e) {
			e.printStackTrace();
		}
	}
	
	public static void updateExl(String filePath) {
		try {
			String tempFileName=System.getProperty("user.dir")+configFileReader.getExcelPath()+configFileReader.getExcelName();
			FileInputStream inp=new FileInputStream(tempFileName);
			Workbook wb=WorkbookFactory.create(inp);
			Sheet sheet=wb.getSheet(configFileReader.getXLSheetName("blue"));
			
			if(System.getProperty("custType").equalsIgnoreCase("gold")) {
				sheet=wb.getSheet(configFileReader.getXLSheetName("gold"));
			}
			Row row=sheet.getRow(Integer.parseInt(System.getProperty("Iteration")));
			Cell cell=row.getCell(5);
			if(cell==null)
				cell=row.createCell(5);
			cell.setCellValue(System.getProperty("PayeeName"));
			
			Cell cell1=row.getCell(6);
			if(cell1==null)
				cell1=row.createCell(6);
			cell.setCellValue(System.getProperty("PayeeNName"));
			
			//Write the output to file
			FileOutputStream fileout=new FileOutputStream(tempFileName);
			wb.write(fileout);
			fileout.close();
			System.out.println(" is successfully written");
			log.info(">> is successfully written");
		}catch(Exception e){
			
		}
	}
	
	public static void setContentExcelFileSheet(String testDataExcelName,String testDataSheet) {
		//Creating the content test data full path
		contentDataExcelPath="/src/test/java/contentCopyDeck/";
		contentDataExcelSheet=testDataSheet.toUpperCase();
		contentDataExcelName=testDataExcelName;
		contentDataPath=currentDir+contentDataExcelPath+contentDataExcelName;
		log.info("Fetching test data for content valifdation from: "+contentDataPath);
		try {
			FileInputStream inputStream=new FileInputStream(new File(contentDataPath));
			try {
				contentExcelWBook=new XSSFWorkbook(inputStream);
				contentExcelWSheet=contentExcelWBook.getSheet(contentDataExcelSheet);				
			}catch(IOException e) {
				e.printStackTrace();
			}
		}catch(FileNotFoundException e) {
			e.printStackTrace();
		}
	}
	
	public static XSSFRow getExcelFileSheetRow() {
		return contentExcelWSheet.getRow(1);
	}
	
	public static String getContentValue(String content_ID, String languageCode) {
		String content_Value=null;
		log.info("Total Row Count:" + contentExcelWSheet.getLastRowNum());
		for(int i=0;i<=contentExcelWSheet.getLastRowNum();i++) {
			contentRow=contentExcelWSheet.getRow(1);
			if(contentRow.getCell(0).getStringCellValue().equalsIgnoreCase(content_ID)) {
				if(languageCode.equalsIgnoreCase("EN"))
					content_Value=contentRow.getCell(1).getStringCellValue();
				else if(languageCode.equalsIgnoreCase("SP"))
					content_Value=contentRow.getCell(2).getStringCellValue();
				else
					Assert.fail("Test Data parameterisation issue");
				log.info("CONTENT_ID:"+content_ID+"-->>"+"CONTENT_VALUE"+content_Value);
				break;
			}
			
		}
		return content_Value;
	}
}
