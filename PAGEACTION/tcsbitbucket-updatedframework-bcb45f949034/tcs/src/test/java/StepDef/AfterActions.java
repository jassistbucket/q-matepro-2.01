package StepDef;

import org.testng.log4testng.Logger;

import cucumber.api.Scenario;
import cucumber.api.java.After;
import util.SeleniumDriver;
import util.SeleniumHelper;

public class AfterActions {
	Logger log=Logger.getLogger(AfterActions.class);
	public static Scenario scenario;
	
	@After
	public void tearDown(Scenario scenario) throws Exception{
		
		if(scenario.isFailed()) {
			SeleniumHelper.getscreenshot("Final step");
		}
		else
			SeleniumHelper.getscreenshot("Final step");
		
		SeleniumDriver.tearDown();
		log.info("---------------------------------");
		String scenarioStatus;
		if(scenario.getStatus().equals("passed")) {
			scenarioStatus="Passed";
		}
		else
			scenarioStatus="Failed";
		//SeleniumHelper.scenarioList.add(new String[] {scenario.getName(),scenarioStatus});
	}
}
