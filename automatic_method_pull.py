# -*- coding: utf-8 -*-
"""
Created on Wed May 29 13:02:44 2019

@author: 1437320
"""

from fuzzywuzzy import fuzz
import collections
from itertools import islice
import pandas as pd
import numpy as np

def read_data():
    
    #query = engine.execute("select repo,filename,code,path from javabot")
    data = pd.read_excel('java_given_when_then.xls', index_col=None, header=None)
    #data = pd.DataFrame(query.fetchall())
    #data.columns = query.keys()
    #conn.close()
    #engine.dispose()
    data[0] = data[0].astype(str)
    #data['code'] = data['code'].astype(str)
    np.savetxt(r'java_when_then.txt', data.values, fmt='%s')
    #data = preprocess_status(data)
    print(" [X] data read succesfully...")
    print()
    print(data.info())

    return data

def read_the_file(L):
    text=L
    method_dict={}
    method_list=[]
    temp_method_list=text.split('@')
    #print(temp_method_list)
    for i in temp_method_list:
        if ('Then(' in i) or('Given(' in i) or('When(' in i) or('And(' in i):
            method_list.append(i)
    
    for i in method_list:
        key=''
        data=''
        first_line=1
        for j in i:
            if first_line==1:
                key=key+j
            if j=='\n':
                first_line=2
            if first_line==2:
                data=data+j
                #print('a')
        method_dict[key]=data
    print("METHODDDDDD",method_dict)
    return method_dict



def take(n, iterable):
    "Return first n items of the iterable as a list"
    return list(islice(iterable, n))   

    
def get_the_function(generated_method_dict,method_dict):
    d1={}
    d2={}
    d3={}
    func_given=pd.DataFrame()
    func_when=pd.DataFrame()
    func_then=pd.DataFrame()
    print("AHSASHHHHHHHHHHHHHHH\n\n\n\n\n",generated_method_dict,method_dict)
    for key1 in generated_method_dict:
        d={}
        for key2 in method_dict:
            
            if key1.startswith('Given(') and key2.startswith('Given('):
                
                Token_Sort_Ratio = fuzz.token_sort_ratio(key1,key2)
                if Token_Sort_Ratio>80:
                    #d1={key1:{Token_Sort_Ratio:method_dict[key2]}}
                    d1={key1:method_dict[key2]}
                    func_given=func_given.append(d1,ignore_index=True)
            elif key1.startswith('When(') and key2.startswith('When('):
                Token_Sort_Ratio=fuzz.token_sort_ratio(key1,key2)
                if Token_Sort_Ratio>80:
                    #d2={key1:{Token_Sort_Ratio:method_dict[key2]}}
                    d2={key1:method_dict[key2]}
                    func_when=func_when.append(d2,ignore_index=True)
            elif key1.startswith('Then(') and key2.startswith('Then('):
                Token_Sort_Ratio=fuzz.token_sort_ratio(key1,key2)
                if Token_Sort_Ratio>80:
                    #d3={key1:{Token_Sort_Ratio:method_dict[key2]}}
                    d3={key1:method_dict[key2]}
                    func_then=func_then.append(d3,ignore_index=True)
            
            
    return func_given,func_when,func_then    


    
    
def remove_import(method):
    
    final_method_list=[]
    
    for i in method:
        final_method=''
        for j in i.split('\n'):
            if len(j)==0:
                continue
            if 'package steps;' in j:
                break
            else:
                final_method=final_method+j+'\n\n'
        final_method_list.append(final_method)
    return final_method_list
            
    
#conn,engine=db_connection()   
data=read_data()
method_dict=read_the_file(open("java_when_then.txt").read())

def automatic_main(generated_method):
    generated_method_dict=read_the_file(generated_method)
    given,when,then=get_the_function(generated_method_dict,method_dict)
    
    given_dict={}
    for key in generated_method_dict:
        g=0
        for col in given.columns:
            if key==col:
                temp_list=list(given[col][pd.notnull(given[col])])
                import_removed=remove_import(temp_list)
                import_removed.insert(0,generated_method_dict[key])
                given_dict[col]= import_removed
                g=1
        if g!=1 and "Given(" in key:
            temp_list=[]
            temp_list.insert(0,generated_method_dict[key])
            print("g!=1",temp_list)
            given_dict[key]=temp_list

    when_dict={}
    for key in generated_method_dict:
        w=0
        for col in when.columns:
            if key==col:
                temp_list=list(when[col][pd.notnull(when[col])])
                import_removed=remove_import(temp_list)
                import_removed.insert(0,generated_method_dict[key])
                when_dict[col]= import_removed
                w=1
        if w!=1 and "When(" in key:
            temp_list=[]
            temp_list.insert(0,generated_method_dict[key])
            print("w!=1",temp_list)
            when_dict[key]=temp_list


    then_dict={}
    for key in generated_method_dict:
        t=0
        for col in then.columns:
            if key==col:
                temp_list=list(then[col][pd.notnull(then[col])])
                import_removed=remove_import(temp_list)
                import_removed.insert(0,generated_method_dict[key])
                then_dict[col]= import_removed
                t=1
                
        if t!=1 and "Then(" in key:
            temp_list=[]
            temp_list.insert(0,generated_method_dict[key])
            print("t!=1",temp_list)
            then_dict[key]=temp_list
    print("GIVEN DICT",given_dict)
    print("WHEN_DICT",when_dict)
    print("THEN_DICT",then_dict)
    return given_dict,when_dict,then_dict


